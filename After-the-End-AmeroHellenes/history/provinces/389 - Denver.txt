# 389 - Denver

# County Title
title = c_denver

# Settlements
max_settlements = 6
b_denver_CO = castle #castle
b_arvada_CO = castle
b_aurora_CO = temple
b_littleton_CO = city

# Misc
culture = kolorantoi
religion = orthodoxy
terrain = mountain
