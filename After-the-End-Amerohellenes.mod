name = "After the End Fan Fork - AmeroHellenes"
path = "mod/After-the-End-AmeroHellenes"

dependencies = { 
	"After the End Fan Fork" 
	"After the End Fan Fork - Americanist Flavor" 
	"After The End - Guruism"
	"After the End Fan Fork - Canada Expanded"
	"After the End Fan Fork - Wild Wasteland"
}
picture = "Apatriarch.png"

tags = { "After the End" 
		"Wild Wild Wasteland"
		"Greek"

}