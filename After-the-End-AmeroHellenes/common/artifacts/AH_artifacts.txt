crown_arcadia = { 
	monthly_character_prestige = 1.5
	monthly_character_piety = 1.5
	stewardship = 2
	diplomacy = 2
	quality = 5
	active = { 
		is_feudal = yes
		has_landed_title = e_arcadia
	}
	flags = { religious unique crown jewelry crown_jewel }
	picture = "GFX_crown_arcadia"
	slot = crown
	stacking = no
	indestructible = yes
	allowed_gift = {
		always = no
	}
}